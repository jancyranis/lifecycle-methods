import React, { useState, useEffect } from "react";
import { Button } from "react-bootstrap";

import "./Useeffect.css";

let grown = false;
function Counter(props) {
  const [growth, setGrowth] = useState(0);

  useEffect(() => {
    console.log("mine grown", props);
    setGrowth(props.value);
  }, []);

  useEffect(() => {
    if (grown) {
      console.log("fall and get to learn");
    } else {
      grown = true;
    }
    return function cleanup() {
      console.log("cleanup after fall");
    };
  });

  function grownChange() {
    setGrowth(growth + 10);
  }

  function grownClean() {
    setGrowth(0);
  }

  return (
    <div class="Useeffect">
      <p>Hooks UseEffect</p>
      <h3>Grown:{growth}</h3>
      <br></br>
      <br></br>
      <center>
        <Button variant="success" type="submit" onClick={grownChange}>
          Grow
        </Button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <Button variant="success" type="cancel" onClick={grownClean}>
          Clean
        </Button>
      </center>
    </div>
  );
}
export default Counter;
