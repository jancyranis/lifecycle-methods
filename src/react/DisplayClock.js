import React,{Component} from 'react';
import './Displayclock.css';
import Clock from './Clock';
export default class Displayclock extends Component{
    constructor(props){
        super(props)
        this.state={
            show:true
        };
    }
    deleteClock=()=>{
        this.setState({
            show: !this.state.show,
        });

    };
    render(){
        return(
            <div class="clock">
                { this.state.show? <Clock/>:<h2>the time clock has been removed</h2>}
                <br />
                    
                    <button onClick={this.deleteClock} type="button" class="btn btn-outline-danger">deleteClock</button>

        
            </div>
        );
    }
}
