import React from "react";
import "./login.css";
import Table from "./Table";
import Validation from "./validation";
import { Button, Form } from "react-bootstrap";
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      useremail: "",
      userpassword: "",
      errors: "",
      tableData: [],
    };
  }

  handleUsermail = (event) => {
    this.setState({
      errors: Validation(event.target.name, event.target.value),
    });
    this.setState({ [event.target.name]: event.target.value });
  };
  onSubmit = (event) => {
    event.preventDefault();
    let tableData = [...this.state.tableData];
    tableData.push({
      useremail: this.state.useremail,
      userpassword: this.state.userpassword,
    });
    this.setState({ tableData, useremail: "", userpassword: "" });
    console.log(this.state.useremail);
  };

  render() {
    const { useremail, userpassword } = this.state;
    return (
      <div>
        <h1>class component login-form</h1>
        <Form className="jancy">
          <marquee>
            <h1>User Login Page</h1>
          </marquee>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email </Form.Label>
            <Form.Control
              placeholder="Enter email"
              name="useremail"
              value={useremail}
              onChange={this.handleUsermail}
            />
          </Form.Group>
          {this.state.errors && (
            <p className="errormessage"> {this.state.errors.erroremail}</p>
          )}

          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control
              placeholder="Password"
              name="userpassword"
              value={userpassword}
              onChange={this.handleUsermail}
            />
          </Form.Group>
          {this.state.errors && (
            <p className="errormessage"> {this.state.errors.errorpassword}</p>
          )}

          <center>
            <Button variant="success" type="submit" onClick={this.onSubmit}>
              Submit
            </Button>
          </center>
        </Form>
        <hr></hr>
        <Table tableData={this.state.tableData} />
      </div>
    );
  }
}
export default Login;
