import React from 'react';
class Table extends React.Component {
 
    render() {
        let tableData= this.props.tableData
        return (
            <div class="info">
                <table>
                    <marquee><h1>Login user-details</h1></marquee>
                    <tr>
                        <th>UserMail</th>
                        <th>UserPassword</th>
                    </tr>
                    <tbody>
                        {tableData.map(data => {
                            return (
                                <tr>
                                    <td>{data.useremail}</td>
                                    <td>{data.userpassword}</td>
                                </tr>
                            );
                        } )}
                    </tbody>
                </table>
            </div>

        )
    }
}
export default Table;