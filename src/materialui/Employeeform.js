import React from 'react'
import './Employeeorm.css'
import Typography from "@material-ui/core/Typography";
import { Form, Row, Col } from 'react-bootstrap';
import Button from "@material-ui/core/Button";
import { Grid } from '@material-ui/core';

function Employeeform() {
    
    return (
        <>
        <div className="styling">
            <div className="text">
            <Typography variant="h5" color="secondary" align="center">
                Employee Information
      </Typography>
      </div>
            <Form>
            <Row className="mb-3">
                    <Form.Group as={Col} md={6} sm={12} controlId="formGridEmail">
                        <Form.Label className="label">First Name</Form.Label>
                        <Form.Control type="name" placeholder="enter firstname" required />
                    </Form.Group>

                    <Form.Group as={Col} md={6} sm={12} controlId="formGridPassword">
                        <Form.Label className="label">Last Name</Form.Label>
                        <Form.Control type="name" placeholder="enter lastname" required  />
                    </Form.Group>
                </Row>

                <Row className="mb-3">
                    <Form.Group as={Col} md={6} sm={12} controlId="formGridEmail">
                        <Form.Label className="label">Email</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" required />
                    </Form.Group>

                    <Form.Group as={Col} md={6} sm={12} controlId="formGridPassword">
                        <Form.Label className="label">Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" required  />
                    </Form.Group>
                </Row>

            <Row>
            <Form.Group className="mb-3" controlId="formGridAddress1">
                    <Form.Label className="label">Address</Form.Label>
                    <Form.Control placeholder="1234 Main St" required />
                </Form.Group></Row>
                
               <Row>
                <Form.Group className="mb-3" controlId="formGridAddress2">
                    <Form.Label className="label">Address 2</Form.Label>
                    <Form.Control placeholder="Apartment, studio, or floor" required />
                </Form.Group>
                </Row>

                <Row className="mb-3">
                    <Form.Group as={Col}  md={6} sm={12} controlId="formGridCity">
                        <Form.Label className="label">City</Form.Label>
                        <Form.Control required />
                    </Form.Group>

                    <Form.Group as={Col} md={6} sm={12} controlId="formGridState">
                        <Form.Label className="label">State</Form.Label>
                        <Form.Select>
                                <option value="Tamil Nadu">Tamil Nadu</option>
                                <option value="Andhra Pradesh">Andhra Pradesh</option>
                                <option selected value="Arunachal Pradesh">Arunachal Pradesh</option>
                                <option value="Assam">Assam</option>
                                <option value=" Bihar"> Bihar</option>
                        </Form.Select>
                    </Form.Group>


                </Row>

                <Grid xs={3} item>

                    <Button Varient="contained" component="label" >
                        upload<inupt type="file" show></inupt>


                    </Button>
                    </Grid> 

                <center><Button variant="contained" color="secondary">
                    Submit
                 </Button> </center>
            </Form>
        </div>
</>
    )
}
export default Employeeform;