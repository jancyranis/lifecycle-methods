import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
// import Button from "@material-ui/core/Button";
import AcUnitIcon from '@material-ui/icons/AcUnit';
// import SendIcon from "@material-ui/icons/Send";
// import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import MaterialTable from 'material-table';
function Uitable() {
  const [data, setData] = useState([
    { name: "jancy", email: "jancyrani@10decoders.in", phone: 9807867656, age: null, gender: "F", city: "villupuram", salary: 12000 },
    { name: "kavin", email: "kavin@10decoders.in", phone: 9987675601, age: 24, gender: "M", city: "chennai", salary: 13000 },
    { name: "guna", email: "guna@10decoders.in", phone: 7807689785, age: null, gender: "M", city: "vellore", salary: 14000 },
    { name: "siva", email: "siva@10decoders.in", phone: 7865042389, age: 25, gender: "M", city: "selam", salary: 15000 },
    { name: "praveen", email: "praveen@10decoders.in", phone: 7865489878, age: 26, gender: "M", city: "yellagiri hills", salary: 16000 }

  ])


  const columns = [
    { title: 'Name', field: 'name', filterPlaceholder: "filter by email", cellStyle: { color: "red" }, headerStyle: { color: "blue" } },
    { title: 'Age', field: 'age', emptyValue: () => <em>null</em>, headerStyle: { color: "blue" } },
    { title: 'City', field: 'city', headerStyle: { color: "blue" } },
    { title: 'Email', field: 'email', headerStyle: { color: "blue" } },
    { title: 'Gender', field: 'gender', lookup: { M: "Male", F: "Female" }, headerStyle: { color: "blue" } },
    { title: 'Phone', field: 'phone', headerStyle: { color: "blue" } },
    { title: 'Salary', field: 'salary', type: "currency", currencySetting: { currencyCode: "INR" }, headerStyle: { color: "blue" } },


  ]



  return (


    <div>
      <Typography variant="h1" color="secondary">
        UI Table Design..
      </Typography>

      <MaterialTable title="User Information "
        data={data}
        columns={columns}


        editable={{
          onRowAdd: newData =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                setData([...data, newData]);

                resolve();
              }, 1000)
            }),
          onRowUpdate: (newData, oldData) =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                const dataUpdate = [...data];
                const index = oldData.tableData.id;

                dataUpdate[index] = newData;
                setData([...dataUpdate]);

                resolve();
              }, 1000)
            }),
          onRowDelete: oldData =>
            new Promise((resolve, reject) => {
              setTimeout(() => {
                const dataDelete = [...data];
                const index = oldData.tableData.id;
                dataDelete.splice(index, 1);
                setData([...dataDelete]);

                resolve()
              }, 1000)
            }),
        }}
        options={
          {
            search: true,
            // search:false,
            paging: true,
            // paging:false,
            pageSizeOptions: [1, 2, 3, 4, 5, 10, 20, 25, 30, 50, 90, 100],
            // pageSize:3,
            // paginationType:"stepped",
            filtering: true,
            exportButton: true,
            exportAllData: true,
            // searchFieldAlignment:"right",
            searchAutoFocus: true,
            // searchFieldVariant:"outlined",
            addRowPosition: "first", actionsColumnIndex: -1,
            selection: true,
            // selection:false,
            // showSelectAllCheckbox:false,
            // showTextRowsSelected:false,
            grouping: true, columnsButton: true,

          }
        }
      />


      <br></br>
      <br></br>

      <center>
        {/* <Button
          variant="contained"
          color="secondary"
          onClick={() => console.log("you just login")}
          startIcon={<SendIcon />}
        >
          Log In
        </Button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <Button
          variant="outlined"
          color="secondary"
          endIcon={<KeyboardArrowRightIcon />}
        >
          Cancel
        </Button> */}
        <br></br>
        <AcUnitIcon />
        <AcUnitIcon color="secondary" fontSize="large" />
        <AcUnitIcon color="action" fontSize="small" />
        <AcUnitIcon color="error" fontSize="small" />
        <AcUnitIcon color="disabled" fontSize="small" />
        <AcUnitIcon color="error" fontSize="small" />
        <AcUnitIcon color="disabled" fontSize="small" />
      </center>
    </div>
  );
}

export default Uitable;
