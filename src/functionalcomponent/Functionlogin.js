import React from 'react';
import './functionlogin.css';
import Tablefunction from './Tablefunction'
import Validfunction from './Validfunction';
import { Button, Form } from 'react-bootstrap';
import { useState } from 'react';

function Functionlogin(){
    const [entry,setEntry] = useState({
        useremail:"", userpassword:""
    });

    const [errors, setErrors] = useState({});
    const [data, setData] = useState([]);


  const handleChange = (event) => {
    let x=Validfunction(event.target.name, event.target.value)
     
    if(event.target.name=="useremail"){
      setErrors({...errors, erroremail: x.erroremail})
    }else if(event.target.name=="userpassword"){
      setErrors({...errors, errorpassword: x .errorpassword})
    }
      
      setEntry({ ...entry, [event.target.name]: event.target.value });
  }

const onSubmit =(event)=>{
  event.preventDefault();
    data.push({
        useremail: entry.useremail,
        userpassword: entry.userpassword,
      });
      setData([...data, data]);
    }

    return (

      <div>
          <h1>functional component login-form</h1>
        <Form className="rani">
         <marquee><h1>User Login Page</h1></marquee> 
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email </Form.Label>
            <Form.Control placeholder="Enter email" name="useremail" value={entry.useremail} onChange={handleChange} />
          </Form.Group>
          {errors && <p className="errormessage" > {errors.erroremail}</p>}
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control placeholder="Password" name="userpassword" value={entry.userpassword} onChange={handleChange} />

          </Form.Group>
          {errors && <p className="errormessage" > {errors.errorpassword}</p>}

          <center><Button variant="success" type="submit" onClick={onSubmit}  >
            Submit
  </Button></center>
        </Form>
        <hr></hr>
        <Tablefunction data={data} />

      </div>
    )
  }
export default Functionlogin;