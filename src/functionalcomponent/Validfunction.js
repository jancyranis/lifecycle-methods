const Validfunction = (name, value) => {
    var errors = {};

    const strongRegex = new RegExp(
        "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
    );


    if (name == "useremail") {
        if (!value) {
            errors.erroremail = "Email is required.";
        } else if (!/\S+@\S+\.\S+/.test(value)) {
            errors.erroremail = "Enter a valid Email Id";
        }
    }
    if (name == "userpassword") {
        if (!value) {
            errors.errorpassword = "Password is required.";
        } else if (!strongRegex.test(value)) {
            errors.errorpassword =
                "Password must be more than 8 characters,must have special symbols and numbers";
        }
    }

    return errors;
};

export default Validfunction;
