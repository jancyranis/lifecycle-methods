import React from "react";
import Header from "./components/header/header";
import Footer from "./components/footer/footer"
import Login from "./components/login/login";
// import Register from "./components/register/register";
const Router = (props) => {
  return (
    <>
      <div>
        <Header />
        
        <Login/>
        {/* <Register/> */}
        
        <Footer />
        
      </div>
    </>
  );
};
export default Router;
