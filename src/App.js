import React from 'react';
import './App.css';
import Displayclock from './react/DisplayClock';
import Login from './react/Login';
import Functionlogin from './functionalcomponent/Functionlogin';
import Counter from './hooks/Counter';
import Uitable from './materialui/Uitable';
import Employeeform from './materialui/Employeeform';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <Router>
      <div className="topnav">
        <nav>
          <ul>
            <li>
              <Link to="/">lifecycle</Link>
            </li>
            <li>
              <Link to="/about">class component login-form</Link>
            </li>
            <li>
              <Link to="/users">functional component login-form</Link>
            </li>
            <li>
              <Link to="/counter">UseEffect-counter</Link>
            </li>
            <li>
              <Link to="/hello">Uitable</Link>
            </li>
            <li>
              <Link to="/employeeform">Employeeform</Link>
            </li>

            


          </ul>
        </nav>
        <Switch>
          {<Route path="/about">
            <Login />
          </Route>}

          {<Route path="/users">
            <Functionlogin />
          </Route>}

          {<Route path="/counter">
            <Counter value={10} />
          </Route>}

          {<Route path="/hello">
            <Uitable />
          </Route>}

          {<Route path="/employeeform">
            <Employeeform />
          </Route>}

          
         



          <Route path="/">
            <Displayclock />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}






export default App;
